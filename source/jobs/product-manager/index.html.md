---
layout: job_page
title: "Product Manager"
---

We're looking for product managers that can help us work on the future of
developer tools. Specifically we're looking for independent people that are
interested in building solutions for (very) large teams and organizations.

We know there are a million things we can and want to improve about GitLab.
It'll be your job to work out what we are going to do and how.

We work in quite a unique way at GitLab, where lots of flexibility and
independence is mixed with a high paced, pragmatic way of working. And
everything we do is in the open.

We recommend looking at our [primer](https://about.gitlab.com/primer)
to get started.

## Responsibilities

- Build an attractive roadmap together with the VP of Product and Head of Product
- Work out feature proposals from the community and customers
- Manage new features tailored for larger enterprises from conception to market
- Work together with UX and Frontend engineers on improving the look and feel of GitLab
- Ensure a smooth release of changes and features together with all stakeholders
- Empower the community by writing great documentation and highlighting the product in various ways in cooperation with marketing
- Finding the weak spots of GitLab and executing plans to improve them
- Work together with other stakeholders on scheduling and executing releases

## You are _not_ responsible for

- Shipping in time. As a PM you are part of a team that delivers a change,
the team is responsible for shipping in time, not you.
- A team of engineers. PMs at GitLab do not manage people, they manage the
_product_. You'll be required to take the lead in decisions about the product,
but it's not your role to manage the people that build the product.
- Capacity and availability planning. You will work together with engineering
managers on schedules and planning: you prioritize, the engineering managers
determine how much can be scheduled.

## Requirements

- Experience in product management
- Strong understanding of Git and Git workflows
- Knowledge of the developer tool space
- Strong technically. You understand how software is built, packaged and deployed.
- Passion for design and usability
- Highly independent and pragmatic
- You are living wherever you want
- You share our [values](/handbook/#values), and work in accordance with those values.
- Bonus points: experience with GitLab
- Bonus points: experience in working with open source projects
