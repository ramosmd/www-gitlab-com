---
layout: job_page
title: "UX Designer"
---

## Responsibilities

* Create wireframes/mockups/clickable deliverables to show the transitions and interactions of new features
* Improve the interface of GitLab
* Work with developers to improve flows
* Conduct user testing
- latest UI/UX techniques, prototyping with tools or HTML/CSS, user testing, user flow.

## Tools

* UI/UX: Adobe CC, Sketch, Antetype, Web typography, assets
* Prototyping: Framer, Origami by Facebook, Principal for Mac, HTML/CSS/JS prototyping only.

## Workflow

* You work on issues tagged with 'UX' on [CE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux) and [EE](https://gitlab.com/gitlab-org/gitlab-ce/issues?label_name=ux).
* When done with an UX issue remove the UX label and add the next [workflow label](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/PROCESS.md#workflow-labels) which is probably the 'Frontend' label.
* Also see the [basics of GitLab development in the developer onboarding](https://about.gitlab.com/handbook/developer-onboarding/#basics-of-gitlab-development).

## Success Criteria

You know you are doing a good job as a UX Designer when:

* You are resolving UX / UI issues assigned to milestones well before the milestone comes up.
* You communicate well with the developers.
* You are contributing ideas and solutions beyond existing issues.
* Users are overwhelmingly happy about your contributions.
* You collaborate effectively with Frontend Engineers, Developers, and Designers.

## Roles in practice

* Andriy is mostly working in close collaboration with Dmitriy on his initiatives.
* Others are mostly working on the items in the milestone and making mockups for new features.
* All are learning HTML and CSS to have deliverables that are a good draft for the implementation.

### UX Interview Questions <a name="ux-interview-questions"></a>

The UX Interview determines if a UX Designer is a good fit for GitLab. Here are some questions we might ask:

1. What is your design process?
1. Similarly, what is your flow from idea to deployment?
1. What are some existing designs we can see as an example of you work?
1. When you did `x` project, what was the biggest problem that you had to solve and how did you solve it?
